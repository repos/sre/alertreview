import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from IPython.display import display, Markdown
import ipywidgets as widgets
import textwrap
import json
import os
from typing import Any, Dict, Tuple, Callable
import pandas.io.formats.style

# Module-level constants and configuration
COLORMAP = "viridis"
MAX_TABLE_ROWS = 10
PLOT_WIDTH = 8
PLOT_HEIGHT_BAR = 3
PLOT_HEIGHT_HEATMAP = 8
PLOT_HEIGHT_GANTT = 3
PLOT_WIDTH_SCATTER = 30  # Slightly bigger width for scatterplot
PLOT_HEIGHT_SCATTER = 4  # Increased height for scatterplot

# Widgets for file paths
file_widgets = {
    "incident": widgets.Text(
        value="data/incident_analysis.json",
        placeholder="Enter incident file path",
        description="Incident:",
        disabled=False,
    ),
    "email": widgets.Text(
        value="data/incident_emails.json",
        placeholder="Enter email file path",
        description="Emails:",
        disabled=False,
    ),
    "icinga_lonely": widgets.Text(
        value="data/icinga_lonely.json",
        placeholder="Enter icinga lonely file path",
        description="Icinga Lonely:",
        disabled=False,
    ),
    "alertmanager": widgets.Text(
        value="data/alertmanager_alerts.csv",
        placeholder="Enter alertmanager file path",
        description="Alertmanager:",
        disabled=False,
    ),
    "icinga": widgets.Text(
        value="data/icinga_alerts.csv",
        placeholder="Enter icinga file path",
        description="Icinga:",
        disabled=False,
    ),
}

# Display widgets for file paths
display(*file_widgets.values())


def validate_file_path(filepath: str) -> bool:
    """
    Validate the provided file path.

    Parameters:
    filepath (str): The path to the file.

    Returns:
    bool: True if the file path is valid, raises an error otherwise.

    Raises:
    FileNotFoundError: If the file does not exist.
    ValueError: If the file type is not supported.
    """
    if not os.path.exists(filepath):
        raise FileNotFoundError(f"The file {filepath} does not exist.")
    if not (filepath.endswith(".json") or filepath.endswith(".csv")):
        raise ValueError("Unsupported file type. Only JSON and CSV are supported.")
    return True


def load_data(filepath: str) -> Any:
    """
    Load JSON or CSV data based on file extension.

    Parameters:
    filepath (str): The path to the file.

    Returns:
    Any: Loaded data, either as a dictionary (for JSON) or DataFrame (for CSV).
    """
    validate_file_path(filepath)
    if filepath.endswith(".json"):
        with open(filepath, "r") as file:
            return json.load(file)
    return pd.read_csv(filepath)


def style_dataframe(df: pd.DataFrame) -> pd.io.formats.style.Styler:
    """
    Apply consistent styling to DataFrame.

    Parameters:
    df (pd.DataFrame): The DataFrame to style.

    Returns:
    pd.io.formats.style.Styler: The styled DataFrame.
    """
    return (
        df.style.hide(axis="index")
        .set_properties(**{"text-align": "left"})
        .set_table_styles(
            [
                {
                    "selector": "th",
                    "props": [("font-weight", "bold"), ("text-align", "left")],
                },
                {"selector": "td:first-child", "props": [("font-weight", "bold")]},
            ]
        )
    )


def display_dataframe_with_widgets(
    df: pd.DataFrame, max_rows: int = MAX_TABLE_ROWS
) -> None:
    """
    Display DataFrame with widgets if it exceeds max rows.

    Parameters:
    df (pd.DataFrame): The DataFrame to display.
    max_rows (int): Maximum number of rows to display without widgets. Defaults to MAX_TABLE_ROWS.
    """
    if len(df) > max_rows:
        output = widgets.Output()
        with output:
            display(style_dataframe(df))
        accordion = widgets.Accordion(children=[output])
        accordion.set_title(0, "Click to expand table")
        display(accordion)
    else:
        display(style_dataframe(df))


def calculate_duration_seconds(duration: str) -> int:
    """
    Calculate duration in seconds from a duration string.

    Parameters:
    duration (str): The duration string (e.g., '1d 2h 3m 4s').

    Returns:
    int: The duration in seconds.
    """
    return sum(
        int(t[:-1]) * {"d": 86400, "h": 3600, "m": 60, "s": 1}[t[-1]]
        for t in duration.split()
    )


def wrap_text(text: str, width: int = 20) -> str:
    """
    Wrap text to fit within a specified width.

    Parameters:
    text (str): The text to wrap.
    width (int): The width to wrap the text to. Defaults to 20.

    Returns:
    str: The wrapped text.
    """
    return "\n".join(textwrap.wrap(text, width))


def wrap_alert_text(text: str, width: int = 50) -> str:
    """
    Shorten alert text if it exceeds the specified width.

    Parameters:
    text (str): The text to shorten.
    width (int): The width to shorten the text to. Defaults to 50.

    Returns:
    str: The shortened text.
    """
    if len(text) > width:
        return textwrap.shorten(text, width=width, placeholder="...")
    return text


def process_email_data(email_data: Dict[str, Any]) -> Tuple[pd.DataFrame, int]:
    """
    Process email data into DataFrame and calculate total and percentage.

    Parameters:
    email_data (Dict[str, Any]): The email data.

    Returns:
    Tuple[pd.DataFrame, int]: The processed DataFrame and total emails count.
    """
    email_df = pd.DataFrame(email_data)
    total_emails = email_df["count"].sum()
    email_df["Percentage"] = (email_df["count"] / total_emails * 100).round(2).astype(
        str
    ) + "%"
    return email_df, total_emails


def process_alertmanager_data(
    alertmanager_df: pd.DataFrame,
) -> Tuple[pd.DataFrame, int]:
    """
    Process AlertManager data and calculate total and percentage.

    Parameters:
    alertmanager_df (pd.DataFrame): The AlertManager data.

    Returns:
    Tuple[pd.DataFrame, int]: The processed DataFrame and total alerts count.
    """
    alertmanager_df.columns = [col.strip() for col in alertmanager_df.columns]
    alertmanager_df["Percentage"] = (
        alertmanager_df["Count"] / alertmanager_df["Count"].sum() * 100
    ).round(2).astype(str) + "%"
    total_alerts = alertmanager_df["Count"].sum()
    return alertmanager_df, total_alerts


def process_icinga_data(icinga_df: pd.DataFrame) -> Tuple[pd.DataFrame, int]:
    """
    Process Icinga data and calculate total and percentage.

    Parameters:
    icinga_df (pd.DataFrame): The Icinga data.

    Returns:
    Tuple[pd.DataFrame, int]: The processed DataFrame and total alerts count.
    """
    icinga_df.columns = [col.strip() for col in icinga_df.columns]
    icinga_df["Alerts"] = icinga_df["Alerts"].apply(wrap_alert_text)
    icinga_df["Percentage"] = (
        icinga_df["Count"] / icinga_df["Count"].sum() * 100
    ).round(2).astype(str) + "%"
    total_icinga_alerts = icinga_df["Count"].sum()
    return icinga_df, total_icinga_alerts


def process_lonely_icinga_data(icinga_lonely_data: Dict[str, Any]) -> pd.DataFrame:
    """
    Process Icinga lonely data and calculate duration in seconds.

    Parameters:
    icinga_lonely_data (Dict[str, Any]): The Icinga lonely data.

    Returns:
    pd.DataFrame: The processed DataFrame with calculated duration in seconds.
    """
    icinga_lonely_df = pd.DataFrame(
        icinga_lonely_data["status"]["service_status"],
        columns=["duration", "host_name", "service_description"],
    )
    icinga_lonely_df["duration_seconds"] = icinga_lonely_df["duration"].apply(
        calculate_duration_seconds
    )
    icinga_lonely_df = icinga_lonely_df.sort_values(
        by="duration_seconds", ascending=False
    )

    host_counts = icinga_lonely_df["host_name"].value_counts()
    host_indices = {host: 1 for host in host_counts.index}

    for i, row in icinga_lonely_df.iterrows():
        if host_counts[row["host_name"]] > 1:
            icinga_lonely_df.at[i, "host_name"] = (
                f"{row['host_name']} ({host_indices[row['host_name']]})"
            )
            host_indices[row["host_name"]] += 1

    return icinga_lonely_df


def create_section(
    title: str,
    subtitle: str,
    df: pd.DataFrame,
    x_label: str,
    y_label: str,
    insights: str,
    max_rows: int = MAX_TABLE_ROWS,
    include_bar_chart: bool = True,
    custom_graph: Callable[[], None] = None,
) -> None:
    """
    Display a section with a title, subtitle, table, (optional) custom graph or bar chart, and insights.

    Parameters:
    title (str): The section title.
    subtitle (str): The section subtitle.
    df (pd.DataFrame): The DataFrame to display.
    x_label (str): The x-axis label for the bar chart.
    y_label (str): The y-axis label for the bar chart.
    insights (str): The insights text.
    max_rows (int): Maximum number of rows to display without widgets. Defaults to MAX_TABLE_ROWS.
    include_bar_chart (bool): Whether to include a bar chart. Defaults to True.
    custom_graph (Callable[[], None]): Optional custom graph function to call.
    """
    display(Markdown(f"### {title}"))
    display(Markdown(f"#### {subtitle}"))
    display_dataframe_with_widgets(df, max_rows)

    if custom_graph:
        custom_graph()
    elif include_bar_chart:
        plot_bar_chart(df, x_label, y_label, title)

    display(Markdown("#### Insights"))
    display(Markdown(insights))
    display(Markdown("---"))


def plot_bar_chart(df: pd.DataFrame, x_label: str, y_label: str, title: str) -> None:
    """
    Plot a bar chart.

    Parameters:
    df (pd.DataFrame): The DataFrame to plot.
    x_label (str): The x-axis label.
    y_label (str): The y-axis label.
    title (str): The chart title.
    """
    fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PLOT_HEIGHT_BAR))
    sns.barplot(
        data=df,
        x=df.columns[0],
        y=df.columns[1],
        hue=df.columns[0],
        palette=COLORMAP,
        ax=ax,
        legend=False,
    )
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title, loc="left")
    plt.xticks(rotation=45, ha="right")
    plt.show()


def plot_top_5_emails(top_5_emails: pd.DataFrame) -> None:
    """
    Plot the top 5 email subjects as a bar chart.

    Parameters:
    top_5_emails (pd.DataFrame): The top 5 email subjects DataFrame.
    """
    fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PLOT_HEIGHT_BAR))
    sns.barplot(
        data=top_5_emails,
        x="count",
        y="subject",
        hue="subject",
        palette=COLORMAP,
        ax=ax,
        legend=False,
    )
    ax.set_xlabel("Count")
    ax.set_ylabel("Subject")
    ax.set_title("Top 5 Email Subjects", loc="left")
    plt.xticks(rotation=45, ha="right")
    for index, value in enumerate(top_5_emails["count"]):
        ax.text(
            value / 2,
            index,
            str(value),
            color="white",
            ha="center",
            va="center",
            fontweight="bold",
        )
    plt.show()


def plot_top_5_alerts(top_5_alerts: pd.DataFrame) -> None:
    """
    Plot the top 5 AlertManager alerts as a bar chart.

    Parameters:
    top_5_alerts (pd.DataFrame): The top 5 AlertManager alerts DataFrame.
    """
    fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PLOT_HEIGHT_BAR))
    sns.barplot(
        data=top_5_alerts,
        x="Count",
        y="Alerts",
        hue="Alerts",
        palette=COLORMAP,
        ax=ax,
        legend=False,
    )
    ax.set_xlabel("Count")
    ax.set_ylabel("Alerts")
    ax.set_title("Top 5 AlertManager Alerts", loc="left")
    plt.xticks(rotation=45, ha="right")
    for index, value in enumerate(top_5_alerts["Count"]):
        ax.text(
            value / 2,
            index,
            str(value),
            color="white",
            ha="center",
            va="center",
            fontweight="bold",
        )
    plt.show()


def plot_top_5_icinga_alerts(top_5_icinga_alerts: pd.DataFrame) -> None:
    """
    Plot the top 5 Icinga alerts as a bar chart.

    Parameters:
    top_5_icinga_alerts (pd.DataFrame): The top 5 Icinga alerts DataFrame.
    """
    fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PLOT_HEIGHT_BAR))
    sns.barplot(
        data=top_5_icinga_alerts,
        x="Count",
        y="Alerts",
        hue="Alerts",
        palette=COLORMAP,
        ax=ax,
        legend=False,
    )
    ax.set_xlabel("Count")
    ax.set_ylabel("Alerts")
    ax.set_title("Top 5 Icinga Alerts", loc="left")
    plt.xticks(rotation=45, ha="right")
    for index, value in enumerate(top_5_icinga_alerts["Count"]):
        ax.text(
            value / 2,
            index,
            str(value),
            color="white",
            ha="center",
            va="center",
            fontweight="bold",
        )
    plt.show()


def plot_lonely_icinga_gantt(icinga_lonely_df: pd.DataFrame) -> None:
    """
    Plot the loneliest Icinga alerts as a Gantt chart.

    Parameters:
    icinga_lonely_df (pd.DataFrame): The Icinga lonely alerts DataFrame.
    """
    fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PLOT_HEIGHT_GANTT))
    icinga_lonely_df["Start"] = pd.Timestamp.now() - pd.to_timedelta(
        icinga_lonely_df["duration_seconds"], unit="s"
    )
    icinga_lonely_df["End"] = pd.Timestamp.now()

    for _, row in icinga_lonely_df.iterrows():
        ax.plot(
            [row["Start"], row["End"]], [row["host_name"], row["host_name"]], marker="o"
        )
        wrapped_text = wrap_text(row["service_description"], width=30)
        ax.text(
            (row["Start"] + (row["End"] - row["Start"]) / 2),
            row["host_name"],
            wrapped_text,
            verticalalignment="bottom",
            horizontalalignment="center",
            fontsize="small",
        )

    ax.set_xlabel("Time")
    ax.set_ylabel("Host")
    plt.xticks(rotation=45)
    plt.title("Loneliest Icinga Alerts (Gantt Chart)", loc="left")
    plt.show()


def plot_incidents_by_hour_day(incidents_by_hour_day: pd.DataFrame) -> None:
    """
    Plot incidents by hour and day as a scatterplot.

    Parameters:
    incidents_by_hour_day (pd.DataFrame): The incidents by hour and day DataFrame.
    """
    hours_order = [
        f"{h:02d}:00" for h in range(23, -1, -1)
    ]  # Descending order from 23:00 to 00:00
    incidents_by_hour_day["Hour"] = pd.Categorical(
        incidents_by_hour_day["Hour"], categories=hours_order, ordered=True
    )

    fig, ax = plt.subplots(figsize=(PLOT_WIDTH_SCATTER, PLOT_HEIGHT_SCATTER))
    sns.scatterplot(
        data=incidents_by_hour_day,
        x="Day",
        y="Hour",
        size="Incidents",
        sizes=(20, 200),
        hue="Incidents",
        palette=COLORMAP,
        legend=False,
        ax=ax,
    )
    ax.grid(True, which="both", color="lightgrey", linewidth=0.5)
    plt.xticks(rotation=90)
    plt.yticks(rotation=0)
    ax.set_xlim(-0.5, len(incidents_by_hour_day["Day"].unique()) - 0.5)
    plt.title("Scatterplot of Paging Alerts by Hour and Day", loc="left")
    plt.show()


def display_email_data(email_df: pd.DataFrame, total_emails: int) -> None:
    """
    Display email data as a table and bar chart.

    Parameters:
    email_df (pd.DataFrame): The email data DataFrame.
    total_emails (int): The total number of emails.
    """
    display(Markdown(f"### Total Emails: {total_emails}"))
    top_5_emails = email_df.nlargest(5, "count")
    create_section(
        title="Emails by Subject",
        subtitle="Frequency of Email Subjects",
        df=email_df.sort_values(by="count", ascending=False),
        x_label="Count",
        y_label="Subject",
        insights=(
            f"The most frequent email subject is '{email_df.sort_values(by='count', ascending=False).iloc[0]['subject']}' "
            f"representing {email_df.sort_values(by='count', ascending=False).iloc[0]['Percentage']} of total emails.<br><br>"
            f"The top 5 alerts account for {email_df.nlargest(5, 'count')['count'].sum()}, "
            f"representing {email_df.nlargest(5, 'count')['count'].sum() / total_emails * 100:.2f}% of the total email alerts ({total_emails})."
        ),
        include_bar_chart=False,
        custom_graph=lambda: plot_top_5_emails(top_5_emails),
    )


def display_alertmanager_data(alertmanager_df: pd.DataFrame, total_alerts: int) -> None:
    """
    Display AlertManager data as a table and bar chart.

    Parameters:
    alertmanager_df (pd.DataFrame): The AlertManager data DataFrame.
    total_alerts (int): The total number of alerts.
    """
    top_5_alerts = alertmanager_df.nlargest(5, "Count")
    create_section(
        title="AlertManager Alerts",
        subtitle="Frequency of AlertManager Alerts",
        df=alertmanager_df.sort_values(by="Count", ascending=False),
        x_label="Count",
        y_label="Alerts",
        insights=(
            f"The most frequent AlertManager alert is '{alertmanager_df.sort_values(by='Count', ascending=False).iloc[0]['Alerts']}' "
            f"representing {alertmanager_df.sort_values(by='Count', ascending=False).iloc[0]['Percentage']} of total alerts.<br><br>"
            f"The top 5 alerts account for {alertmanager_df.nlargest(5, 'Count')['Count'].sum()}, "
            f"representing {alertmanager_df.nlargest(5, 'Count')['Count'].sum() / total_alerts * 100:.2f}% of the total AlertManager alerts ({total_alerts})."
        ),
        include_bar_chart=False,
        custom_graph=lambda: plot_top_5_alerts(top_5_alerts),
    )


def display_icinga_data(icinga_df: pd.DataFrame, total_icinga_alerts: int) -> None:
    """
    Display Icinga data as a table and bar chart.

    Parameters:
    icinga_df (pd.DataFrame): The Icinga data DataFrame.
    total_icinga_alerts (int): The total number of Icinga alerts.
    """
    top_5_icinga_alerts = icinga_df.nlargest(5, "Count")
    create_section(
        title="Icinga Alerts",
        subtitle="Last 90 days",
        df=icinga_df.sort_values(by="Count", ascending=False),
        x_label="Count",
        y_label="Alerts",
        insights=(
            f"The most frequent Icinga alert is '{icinga_df.sort_values(by='Count', ascending=False).iloc[0]['Alerts']}' "
            f"representing {icinga_df.sort_values(by='Count', ascending=False).iloc[0]['Percentage']} of total alerts.<br><br>"
            f"The top 5 alerts account for {icinga_df.nlargest(5, 'Count')['Count'].sum()}, "
            f"representing {icinga_df.nlargest(5, 'Count')['Count'].sum() / total_icinga_alerts * 100:.2f}% of the total Icinga alerts ({total_icinga_alerts})."
        ),
        include_bar_chart=False,
        custom_graph=lambda: plot_top_5_icinga_alerts(top_5_icinga_alerts),
    )


def display_lonely_icinga_data(icinga_lonely_df: pd.DataFrame) -> None:
    """
    Display Icinga lonely alerts as a table and Gantt chart.

    Parameters:
    icinga_lonely_df (pd.DataFrame): The Icinga lonely alerts DataFrame.
    """
    create_section(
        title="Loneliest Icinga Alerts",
        subtitle="Alerts with Longest Duration",
        df=icinga_lonely_df[["duration", "host_name", "service_description"]],
        x_label="Host",
        y_label="Duration",
        insights=(
            f"The top 5 alerts account for {icinga_lonely_df.nlargest(5, 'duration_seconds')['duration_seconds'].sum()}, "
            f"representing {icinga_lonely_df.nlargest(5, 'duration_seconds')['duration_seconds'].sum() / icinga_lonely_df['duration_seconds'].sum() * 100:.2f}% "
            f"of the total loneliest Icinga alerts ({icinga_lonely_df['duration_seconds'].sum()})."
        ),
        include_bar_chart=False,
        custom_graph=lambda: plot_lonely_icinga_gantt(icinga_lonely_df),
    )


def display_incident_analysis_data(incident_data: Dict[str, Any]) -> None:
    """
    Display incident analysis data.

    Parameters:
    incident_data (Dict[str, Any]): The incident analysis data.
    """
    total_incidents = incident_data["total_incidents"]
    display(Markdown(f"### Total Incidents: {total_incidents}"))

    # Display Incidents by Month
    incidents_by_month = pd.DataFrame(
        list(incident_data["incidents_by_month"].items()),
        columns=["Month", "Incidents"],
    )
    incidents_by_month["Percentage"] = (
        incidents_by_month["Incidents"] / total_incidents * 100
    ).round(2).astype(str) + "%"
    create_section(
        title="Paging alerts by Month",
        subtitle="From June 2023 to June 2024",
        df=incidents_by_month,
        x_label="Month",
        y_label="Incidents",
        insights="September 2023 was the month with the most incidents (75, 16.23% of total), October 2023 had the least (14, 3.03% of total).",
    )

    # Display Incidents by Day
    incidents_by_day = pd.DataFrame(
        list(incident_data["incidents_by_day"].items()), columns=["Day", "Incidents"]
    )
    incidents_by_day["Percentage"] = (
        incidents_by_day["Incidents"] / total_incidents * 100
    ).round(2).astype(str) + "%"
    create_section(
        title="Paging alerts by Day",
        subtitle="Distribution of Incidents across the Week",
        df=incidents_by_day,
        x_label="Day",
        y_label="Incidents",
        insights="Thursday had the most incidents (88, 19.05% of total), Friday had the least (42, 9.09% of total).",
    )

    # Display Incidents by Hour
    incidents_by_hour = pd.DataFrame(
        list(incident_data["incidents_by_hour"].items()), columns=["Hour", "Incidents"]
    )
    incidents_by_hour["Percentage"] = (
        incidents_by_hour["Incidents"] / total_incidents * 100
    ).round(2).astype(str) + "%"
    create_section(
        title="Paging alerts by Hour",
        subtitle="Distribution of Incidents across the Day",
        df=incidents_by_hour,
        x_label="Hour",
        y_label="Incidents",
        insights="The hour with the most incidents was 10:00 UTC (41, 8.87% of total), the hour with the least was 4:00 UTC (7, 1.52% of total).",
    )

    # Display Incidents by Week
    incidents_by_week = pd.DataFrame(
        list(incident_data["incidents_by_week"].items()), columns=["Week", "Incidents"]
    )
    incidents_by_week["Week"] = pd.to_datetime(
        incidents_by_week["Week"].apply(lambda x: x.split("starting ")[-1])
    )
    incidents_by_week = incidents_by_week.sort_values(by="Week")
    incidents_by_week["Week"] = incidents_by_week["Week"].dt.strftime("%Y-%m-%d")
    incidents_by_week["Percentage"] = (
        incidents_by_week["Incidents"] / total_incidents * 100
    ).round(2).astype(str) + "%"
    create_section(
        title="Paging alerts by Week",
        subtitle="Distribution of Incidents across the Weeks",
        df=incidents_by_week,
        x_label="Week",
        y_label="Pages",
        insights="Week starting 2024-02-19 had the most incidents (37, 8.01% of total), Week starting 2023-05-29 had the least (1, 0.22% of total).",
    )

    # Display Incidents by Hour and Day as a scatterplot
    incidents_by_hour_day = pd.DataFrame(
        [
            {
                "Day": day,
                "Hour": f"{int(hour.split(':')[0]):02d}:00",
                "Incidents": count,
            }
            for day, hours in incident_data["incidents_by_hour_day"].items()
            for hour, count in hours.items()
        ]
    )

    create_section(
        title="Paging alerts by Hour and Day",
        subtitle="Scatterplot of Paging alerts by Hour and Day",
        df=incidents_by_hour_day,
        x_label="Day",
        y_label="Hour",
        insights=(
            f"Day {incidents_by_hour_day.loc[incidents_by_hour_day['Incidents'].idxmax()]['Day']} at {incidents_by_hour_day.loc[incidents_by_hour_day['Incidents'].idxmax()]['Hour']} had the most incidents "
            f"({incidents_by_hour_day['Incidents'].max()}, {(incidents_by_hour_day['Incidents'].max() / total_incidents * 100):.2f}% of total), "
            f"{incidents_by_hour_day.loc[incidents_by_hour_day['Incidents'].idxmin()]['Day']} at {incidents_by_hour_day.loc[incidents_by_hour_day['Incidents'].idxmin()]['Hour']} had the least incidents "
            f"({incidents_by_hour_day['Incidents'].min()}, {(incidents_by_hour_day['Incidents'].min() / total_incidents * 100):.2f}% of total)."
        ),
        include_bar_chart=False,
        custom_graph=lambda: plot_incidents_by_hour_day(incidents_by_hour_day),
    )


def display_most_least_summary(incident_data: Dict[str, Any]) -> None:
    """
    Display a summary table of the most and least paging alerts by hour, day, week, and month.

    Parameters:
    incident_data (Dict[str, Any]): The incident data.
    """
    summary_data = {
        "Most paging alerts day": incident_data.get(
            "most_incidents_day", {"date": "N/A", "count": 0}
        ),
        "Least paging alerts day": incident_data.get(
            "least_incidents_day", {"date": "N/A", "count": 0}
        ),
        "Most paging alerts week": incident_data.get(
            "most_incidents_week", {"date": "N/A", "count": 0}
        ),
        "Least paging alerts week": incident_data.get(
            "least_incidents_week", {"date": "N/A", "count": 0}
        ),
    }

    summary_df = pd.DataFrame(
        [
            {
                "": key,
                "date": value["date"],
                "count": value["count"],
                "percentage": f"{value.get('percentage', 0):.2f}%",
            }
            for key, value in summary_data.items()
        ]
    )

    display(Markdown("### Summary of Most and Least Paging Alerts"))
    display(style_dataframe(summary_df))


def display_most_frequent_entities(
    incident_data: Dict[str, Any], total_incidents: int
) -> None:
    """
    Display most frequent entities.

    Parameters:
    incident_data (Dict[str, Any]): The incident data.
    total_incidents (int): The total number of incidents.
    """
    most_frequent_entities = pd.DataFrame(
        list(incident_data["most_frequent_entities"].items()),
        columns=["Entity", "Count"],
    ).nlargest(10, "Count")
    most_frequent_entities["Percentage"] = (
        most_frequent_entities["Count"] / total_incidents * 100
    ).round(2).astype(str) + "%"
    top_5_entities = most_frequent_entities.nlargest(5, "Count")
    create_section(
        title="Most Frequent Paging Alerts",
        subtitle="Top 10 Paging Alerts",
        df=most_frequent_entities,
        x_label="Count",
        y_label="Entity",
        insights=(
            f"The top 5 alerts account for {most_frequent_entities.nlargest(5, 'Count')['Count'].sum()}, "
            f"representing {most_frequent_entities.nlargest(5, 'Count')['Count'].sum() / total_incidents * 100:.2f}% of the total frequent entity alerts ({total_incidents})."
        ),
        include_bar_chart=False,
        custom_graph=lambda: plot_top_5_entities(top_5_entities),
    )


def plot_top_5_entities(top_5_entities: pd.DataFrame) -> None:
    """
    Plot the top 5 most frequent entities as a bar chart.

    Parameters:
    top_5_entities (pd.DataFrame): The top 5 most frequent entities DataFrame.
    """
    fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PLOT_HEIGHT_BAR))
    sns.barplot(
        data=top_5_entities,
        x="Count",
        y="Entity",
        hue="Entity",
        palette=COLORMAP,
        ax=ax,
        legend=False,
    )
    ax.set_xlabel("Count")
    ax.set_ylabel("Entity")
    ax.set_title("Top 5 Most Frequent Paging Alerts", loc="left")
    plt.xticks(rotation=45, ha="right")
    for index, value in enumerate(top_5_entities["Count"]):
        ax.text(
            value / 2,
            index,
            str(value),
            color="white",
            ha="center",
            va="center",
            fontweight="bold",
        )
    plt.show()


def main() -> None:
    """
    Main function to run the alert review analysis.
    """
    # Load data from widgets
    incident_data = load_data(file_widgets["incident"].value)
    email_data = load_data(file_widgets["email"].value)
    icinga_lonely_data = load_data(file_widgets["icinga_lonely"].value)
    alertmanager_df = load_data(file_widgets["alertmanager"].value)
    icinga_df = load_data(file_widgets["icinga"].value)

    display(Markdown("# Alert Review"))
    display(Markdown("## Q4"))

    # Process and display email data
    email_df, total_emails = process_email_data(email_data)
    display_email_data(email_df, total_emails)

    # Process and display AlertManager data
    alertmanager_df, total_alerts = process_alertmanager_data(alertmanager_df)
    display_alertmanager_data(alertmanager_df, total_alerts)

    # Process and display Icinga data
    icinga_df, total_icinga_alerts = process_icinga_data(icinga_df)
    display_icinga_data(icinga_df, total_icinga_alerts)

    # Process and display lonely Icinga data
    icinga_lonely_df = process_lonely_icinga_data(icinga_lonely_data)
    display_lonely_icinga_data(icinga_lonely_df)

    # Display incident analysis data
    display_incident_analysis_data(incident_data)

    # Display most frequent entities
    display_most_frequent_entities(incident_data, incident_data["total_incidents"])


# Run the main analysis
if __name__ == "__main__":
    main()
