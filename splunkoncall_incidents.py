import os
import requests
import json
import time
from datetime import datetime, timedelta
from collections import defaultdict, Counter
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Get API credentials from environment variables
API_ID = os.getenv("API_ID")
API_KEY = os.getenv("API_KEY")
BASE_URL = "https://api.victorops.com/api-public/v1/incidents/"

# Ensure the data directory exists
os.makedirs("data", exist_ok=True)

# File paths
RESPONSES_FILE = "data/incident_responses.json"
INFO_FILE = "data/incident_info.json"
ANALYSIS_FILE = "data/incident_analysis.json"

# Policies to include in the analysis
INCLUDED_POLICIES = ["SRE Business Hours (Escalation)", "SRE Batphone (Escalation)"]


def get_incident_data(incident_number: int) -> dict:
    """
    Retrieves incident data from the SplunkOnCall API.

    Args:
        incident_number (int): The incident number to retrieve.

    Returns:
        dict: The incident data if the request is successful, None otherwise.
    """
    headers = {
        "Accept": "application/json",
        "X-VO-Api-Id": API_ID,
        "X-VO-Api-Key": API_KEY,
    }
    response = requests.get(f"{BASE_URL}{incident_number}", headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print(
            f"Failed to retrieve data for incident {incident_number}: {response.status_code}"
        )
        return None


def extract_incident_info(incident: dict) -> dict:
    """
    Extracts relevant information from the incident data.

    Args:
        incident (dict): The incident data.

    Returns:
        dict: The extracted incident information if it meets the criteria, None otherwise.
    """
    start_time = datetime.strptime(incident["startTime"], "%Y-%m-%dT%H:%M:%SZ")
    end_time = None
    acked_by = None
    if "transitions" in incident:
        for transition in incident["transitions"]:
            if transition["name"] == "RESOLVED":
                end_time = datetime.strptime(transition["at"], "%Y-%m-%dT%H:%M:%SZ")
            elif transition["name"] == "ACKED":
                acked_by = transition["by"]

    if not end_time:
        print(f"Incident {incident['incidentNumber']} is not resolved.")
        return None

    duration = end_time - start_time

    policy_names = [
        policy["policy"]["name"]
        for policy in incident["pagedPolicies"]
        if policy["policy"]["name"] in INCLUDED_POLICIES
    ]

    if not policy_names:
        return None

    return {
        "incidentNumber": incident["incidentNumber"],
        "entityDisplayName": incident["entityDisplayName"],
        "startTime": incident["startTime"],
        "endTime": end_time.isoformat(),
        "duration": duration,
        "duration_str": str(duration),
        "escalationPolicies": policy_names,
        "ackedBy": acked_by,
    }


def gather_incidents(start_incident: int, end_incident: int) -> list:
    """
    Gathers incidents within the specified range.

    Args:
        start_incident (int): The starting incident number.
        end_incident (int): The ending incident number.

    Returns:
        list: A list of incident information.
    """
    incidents = []
    for incident_number in range(start_incident, end_incident - 1, -1):
        print(f"Processing incident number: {incident_number}")
        incident_data = get_incident_data(incident_number)
        if incident_data:
            save_response_to_file(incident_data, RESPONSES_FILE)
            incident_info = extract_incident_info(incident_data)
            if incident_info:
                incidents.append(incident_info)
        time.sleep(0.5)  # Ensures we don't exceed 2 API calls per second
    return incidents


def save_response_to_file(response: dict, filename: str) -> None:
    """
    Saves the API response to a file.

    Args:
        response (dict): The API response.
        filename (str): The file to save the response to.
    """
    with open(filename, "a") as f:
        f.write(json.dumps(response) + "\n")


def load_responses_from_file(filename: str) -> list:
    """
    Loads incident data from a file.

    Args:
        filename (str): The file to load the data from.

    Returns:
        list: A list of incident information.
    """
    incidents = []
    with open(filename, "r") as f:
        for line in f:
            try:
                incident_data = json.loads(line)
                incident_info = extract_incident_info(incident_data)
                if incident_info:
                    incidents.append(incident_info)
            except json.JSONDecodeError as e:
                print(f"Error decoding JSON on line: {line}\nError: {e}")
    return incidents


def parse_duration(duration_str: str) -> timedelta:
    """
    Parses a duration string into a timedelta object.

    Args:
        duration_str (str): The duration string.

    Returns:
        timedelta: The parsed duration.
    """
    if "day" in duration_str:
        days, time_str = duration_str.split(", ")
        days = int(days.split()[0])
        hours, minutes, seconds = map(int, time_str.split(":"))
    else:
        days = 0
        hours, minutes, seconds = map(int, duration_str.split(":"))
    return timedelta(days=days, hours=hours, minutes=minutes, seconds=seconds)


def clean_entity_display_name(entity_display_name: str) -> (str, int):
    """
    Cleans the entity display name by removing any multipliers and returns the cleaned name and the multiplier count.

    Args:
        entity_display_name (str): The entity display name with possible multipliers.

    Returns:
        tuple: The cleaned entity display name and the multiplier count.
    """
    multiplier = 1
    if "x]" in entity_display_name:
        parts = entity_display_name.split("x]")
        multiplier = int(parts[0].strip("["))
        entity_display_name = parts[1].strip()
    return entity_display_name, multiplier


def perform_analysis(incidents: list) -> dict:
    """
    Performs analysis on the gathered incidents.

    Args:
        incidents (list): The list of incidents.

    Returns:
        dict: The analysis results.
    """
    total_incidents = len(incidents)
    policy_count = defaultdict(int)
    ack_count_total = defaultdict(int)
    ack_count_batphone = defaultdict(int)
    ack_count_business_hours = defaultdict(int)
    ack_policy_count = defaultdict(lambda: defaultdict(int))
    total_duration = timedelta()
    longest_incident = None
    shortest_incident = None
    incidents_by_day = Counter()
    incidents_by_hour = Counter()
    incidents_by_month = Counter()
    incidents_by_week = defaultdict(int)
    entity_count = Counter()
    incidents_by_hour_day = defaultdict(lambda: defaultdict(int))
    incidents_by_date = Counter()

    for incident in incidents:
        if isinstance(incident["duration"], str):
            incident["duration"] = parse_duration(incident["duration"])

        for policy in incident["escalationPolicies"]:
            policy_count[policy] += 1
            if policy == "SRE Batphone (Escalation)":
                ack_count_batphone[incident["ackedBy"]] += 1
            elif policy == "SRE Business Hours (Escalation)":
                ack_count_business_hours[incident["ackedBy"]] += 1

        ack_count_total[incident["ackedBy"]] += 1

        for policy in incident["escalationPolicies"]:
            ack_policy_count[incident["ackedBy"]][policy] += 1
        total_duration += incident["duration"]

        if (
            longest_incident is None
            or incident["duration"] > longest_incident["duration"]
        ):
            longest_incident = incident
        if (
            shortest_incident is None
            or incident["duration"] < shortest_incident["duration"]
        ):
            shortest_incident = incident

        start_time = datetime.strptime(incident["startTime"], "%Y-%m-%dT%H:%M:%SZ")
        week_number = start_time.isocalendar()[1]
        year = start_time.isocalendar()[0]
        week_start = (start_time - timedelta(days=start_time.weekday())).strftime(
            "%Y-%m-%d"
        )
        incidents_by_week[f"Week {week_number} starting {week_start}"] += 1
        incidents_by_day[start_time.strftime("%A")] += 1
        incidents_by_hour[start_time.hour] += 1
        incidents_by_month[start_time.strftime("%Y-%m")] += 1
        incidents_by_hour_day[start_time.strftime("%Y-%m-%d")][
            f"{start_time.hour}:00 UTC"
        ] += 1
        incidents_by_date[start_time.strftime("%Y-%m-%d")] += 1

        entity_name, multiplier = clean_entity_display_name(
            incident["entityDisplayName"]
        )
        entity_count[entity_name] += multiplier

    average_duration = (
        total_duration / total_incidents if total_incidents else timedelta()
    )

    days_order = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ]
    sorted_incidents_by_day = {
        day: incidents_by_day[day] for day in days_order if day in incidents_by_day
    }

    sorted_incidents_by_hour = {
        f"{hour}:00 UTC": count for hour, count in sorted(incidents_by_hour.items())
    }

    sorted_incidents_by_month = dict(sorted(incidents_by_month.items()))

    sorted_entity_count = dict(
        sorted(entity_count.items(), key=lambda item: item[1], reverse=True)
    )

    sorted_ack_count_total = dict(
        sorted(ack_count_total.items(), key=lambda item: item[1], reverse=True)
    )
    sorted_ack_policy_count = {
        user: dict(sorted(policies.items(), key=lambda item: item[1], reverse=True))
        for user, policies in ack_policy_count.items()
    }
    sorted_ack_policy_count = dict(
        sorted(
            sorted_ack_policy_count.items(),
            key=lambda item: sum(item[1].values()),
            reverse=True,
        )
    )
    sorted_ack_count_batphone = dict(
        sorted(ack_count_batphone.items(), key=lambda item: item[1], reverse=True)
    )
    sorted_ack_count_business_hours = dict(
        sorted(ack_count_business_hours.items(), key=lambda item: item[1], reverse=True)
    )

    most_incidents_day, most_incidents_count = max(
        incidents_by_date.items(), key=lambda item: item[1]
    )
    least_incidents_day, least_incidents_count = min(
        incidents_by_date.items(), key=lambda item: item[1]
    )

    most_incidents_week, most_incidents_week_count = max(
        incidents_by_week.items(), key=lambda item: item[1]
    )
    least_incidents_week, least_incidents_week_count = min(
        incidents_by_week.items(), key=lambda item: item[1]
    )

    sorted_incidents_by_week = dict(
        sorted(incidents_by_week.items(), key=lambda item: item[1], reverse=True)
    )

    analysis = {
        "total_incidents": total_incidents,
        "policy_count": dict(policy_count),
        "average_duration": str(average_duration),
        "ack_count_total": sorted_ack_count_total,
        "ack_count_batphone": sorted_ack_count_batphone,
        "ack_count_business_hours": sorted_ack_count_business_hours,
        "ack_policy_count": sorted_ack_policy_count,
        "longest_incident": {
            "incidentNumber": longest_incident["incidentNumber"],
            "entityDisplayName": longest_incident["entityDisplayName"],
            "duration": str(longest_incident["duration"]),
            "ackedBy": longest_incident["ackedBy"],
        },
        "shortest_incident": {
            "incidentNumber": shortest_incident["incidentNumber"],
            "entityDisplayName": shortest_incident["entityDisplayName"],
            "duration": str(shortest_incident["duration"]),
            "ackedBy": shortest_incident["ackedBy"],
        },
        "incidents_by_month": sorted_incidents_by_month,
        "incidents_by_day": sorted_incidents_by_day,
        "incidents_by_hour": sorted_incidents_by_hour,
        "incidents_by_week": dict(incidents_by_week),
        "incidents_by_week_sorted": sorted_incidents_by_week,
        "incidents_by_hour_day": {
            day: dict(hours) for day, hours in incidents_by_hour_day.items()
        },
        "most_incidents_day": {
            "date": most_incidents_day,
            "count": most_incidents_count,
        },
        "least_incidents_day": {
            "date": least_incidents_day,
            "count": least_incidents_count,
        },
        "most_incidents_week": {
            "week": most_incidents_week,
            "count": most_incidents_week_count,
        },
        "least_incidents_week": {
            "week": least_incidents_week,
            "count": least_incidents_week_count,
        },
        "most_frequent_entities": sorted_entity_count,
    }

    return analysis


def save_analysis_to_file(analysis: dict, filename: str) -> None:
    """
    Saves the analysis results to a file.

    Args:
        analysis (dict): The analysis results.
        filename (str): The file to save the results to.
    """
    with open(filename, "w") as f:
        json.dump(analysis, f, indent=4)


def main(start_incident: int, end_incident: int) -> None:
    """
    Main function to gather incidents and perform analysis.

    Args:
        start_incident (int): The starting incident number.
        end_incident (int): The ending incident number.
    """
    if os.path.exists(RESPONSES_FILE):
        print("Loading data from incident_responses.json...")
        incidents = load_responses_from_file(RESPONSES_FILE)
    else:
        incidents = gather_incidents(start_incident, end_incident)

    if incidents:
        for incident in incidents:
            incident["duration"] = str(incident["duration"])

        # Overwrite the contents of the INFO_FILE
        with open(INFO_FILE, "w") as f:
            json.dump(incidents, f, indent=4)
        print(json.dumps(incidents, indent=4))

        analysis = perform_analysis(incidents)

        # Overwrite the contents of the ANALYSIS_FILE
        with open(ANALYSIS_FILE, "w") as f:
            json.dump(analysis, f, indent=4)
        print(json.dumps(analysis, indent=4))
    else:
        print("No incidents found in the specified range that meet the criteria.")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Retrieve SplunkOnCall incidents.")
    parser.add_argument(
        "start_incident", type=int, help="Incident number to start from."
    )
    parser.add_argument("end_incident", type=int, help="Incident number to stop at.")
    args = parser.parse_args()
    main(args.start_incident, args.end_incident)
